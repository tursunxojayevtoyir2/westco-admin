import "./App.css";
import React, { useEffect, useState } from "react";
import axios from "axios";

function App() {
  const [password, setPassword] = useState("");
  const [data, setData] = useState([]);

  const signIn = () => {
    axios
      .post(`https://westco1.herokuapp.com/employee/sign-in`, { password })
      .then((response) => {
        console.log(response.data.data.token);
        localStorage.setItem("token", response.data.data.token);
      });
    const timer = setTimeout(() => {
      window.location.reload(false);
    }, 3000);
    return () => clearTimeout(timer);
  };
  const token = localStorage.getItem("token");

  axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;
  axios.defaults.headers.post["Content-Type"] =
    "application/x-www-form-urlencoded";
  const onChange = (e) => {
    e.preventDefault();
    setPassword(e.target.value);
  };

  useEffect(() => {
    axios
      .get("https://westco1.herokuapp.com/employee/user?limit=5&page=1")
      .then((response) => {
        console.log(response);
        setData(response?.data?.data?.data);
      });
  }, []);

  const deletePost = (id) => {
    console.log(id);
    axios
      .delete(`https://westco1.herokuapp.com/employee/user/${id}`)
      .then((response) => {
        console.log(response);
      });
      const timer = setTimeout(() => {
        window.location.reload(false);
      }, 2000);
      return () => clearTimeout(timer);
  };

  return (
    <>
      <div className="container">
        <div className="row">
          <div className="col">
            {token ? (
              ""
            ) : (
              <div>
                <div className="d-flex justify-content-center m-5">
                  <input
                    className="form-control w-50"
                    type="text"
                    placeholder="password"
                    onChange={onChange}
                  />
                </div>
                <div className="d-flex justify-content-center">
                  <button className="btn btn-primary" onClick={signIn}>
                    enter
                  </button>
                </div>
              </div>
            )}
            {token ? (
              <table className="table">
                <thead>
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Phone number</th>
                    <th scope="col">Email</th>
                    <th scope="col">Date</th>
                    <th scope="col">Delete</th>
                  </tr>
                </thead>
                <tbody>
                  {data?.map((data, index) => {
                    return (
                      <tr key={data._id}>
                        <th scope="row">
                          {index + 1}
                        </th>
                        <td>{data?.fullName}</td>
                        <td>{data?.phoneNumber}</td>
                        <td>{data?.email}</td>
                        <td>
                          {new Date(data?.createdAt).toLocaleDateString()}
                        </td>
                        <td>
                          <button
                            onClick={() => {
                              deletePost(data._id);
                            }}
                            className="btn btn-danger"
                          >
                            delete
                          </button>
                        </td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            ) : (
              ""
            )}
          </div>
        </div>
      </div>
    </>
  );
}

export default App;
